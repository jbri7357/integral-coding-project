# Integral Kata Exercise

Integral kata exercise (Twitter clone) built using Java, Spring Boot, JUnit, Hibernate, and an H2 in-memory database.

### Setup
To run, use:
```
$ ./gradlew test
```

### Overview
This coding exercise has tweets, users, and follower/followee relationships represented by one entity, repository, and service class each. Current functionality includes the following:

- Create user
- Create tweet
- Get timeline (collection of tweets by user)
- Get wall (collection of tweets by all followed users)
- Create follower / followee relationship
