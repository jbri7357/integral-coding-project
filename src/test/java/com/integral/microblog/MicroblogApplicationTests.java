package com.integral.microblog;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.GregorianCalendar;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.integral.microblog.model.Relationship;
import com.integral.microblog.model.Tweet;
import com.integral.microblog.model.User;
import com.integral.microblog.repository.RelationshipRepository;
import com.integral.microblog.repository.TweetRepository;
import com.integral.microblog.repository.UserRepository;
import com.integral.microblog.service.RelationshipService;
import com.integral.microblog.service.TweetService;
import com.integral.microblog.service.UserService;

@SpringBootTest
class MicroblogApplicationTests {

	@Autowired
	TweetRepository tweetRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	RelationshipRepository relationshipRepository;

	@Autowired
	TweetService tweetService;
	@Autowired
	UserService userService;
	@Autowired
	RelationshipService relationshipService;

	final String ALICE_TWEET_CONTENT = "I love the weather today.";
	final String BOB_TWEET_CONTENT_1 = "Darn! We lost!";
	final String BOB_TWEET_CONTENT_2 = "Good game though.";
	final String CHARLIE_TWEET_CONTENT = "I'm in New York today! Anyone wants to have a coffee?";

	GregorianCalendar calendar = new GregorianCalendar();

	@Test
	void testCreateTweet() {
		final User alice = userService.createUser(new User("alice@gmail.com", "Alice"));

		calendar.set(2021, 8, 15, 15, 0);
		tweetService.postTweet(new Tweet(alice, calendar.getTime(), ALICE_TWEET_CONTENT));

		List<Tweet> timeline = tweetService.getTimelineForUser(alice);

		assertEquals(1, timeline.size(), "Timeline should have an entry");
		assertEquals(ALICE_TWEET_CONTENT, timeline.get(0).getContent(), "Tweet content should be expected value");

	}

	@Test
	void testGetTimeline() {
		final User bob = userService.createUser(new User("bob@gmail.com", "Bob"));

		calendar.set(2021, 8, 15, 15, 3);
		tweetService.postTweet(new Tweet(bob, calendar.getTime(), BOB_TWEET_CONTENT_1));

		calendar.set(2021, 8, 15, 15, 4);
		tweetService.postTweet(new Tweet(bob, calendar.getTime(), BOB_TWEET_CONTENT_2));

		List<Tweet> timeline = tweetService.getTimelineForUser(bob);

		assertEquals(2, timeline.size(), "Timeline should have two entries");
		assertEquals(BOB_TWEET_CONTENT_1, timeline.get(0).getContent(), "Tweet content should be expected value");
		assertEquals(BOB_TWEET_CONTENT_2, timeline.get(1).getContent(), "Tweet content should be expected value");

	}

	@Test
	void testFollowing() {
		final User alice = userService.createUser(new User("alice@gmail.com", "Alice"));
		final User bob = userService.createUser(new User("bob@gmail.com", "Bob"));
		final User charlie = userService.createUser(new User("charlie@gmail.com", "Charlie"));

		calendar.set(2021, 8, 15, 15, 0);
		tweetService.postTweet(new Tweet(alice, calendar.getTime(), ALICE_TWEET_CONTENT));

		calendar.set(2021, 8, 15, 15, 3);
		tweetService.postTweet(new Tweet(bob, calendar.getTime(), BOB_TWEET_CONTENT_1));

		calendar.set(2021, 8, 15, 15, 4);
		tweetService.postTweet(new Tweet(bob, calendar.getTime(), BOB_TWEET_CONTENT_2));

		calendar.set(2021, 8, 15, 15, 5);
		tweetService.postTweet(new Tweet(charlie, calendar.getTime(), CHARLIE_TWEET_CONTENT));

		Relationship charlieFollowsAliceRelationship = new Relationship(charlie, alice);
		Relationship charlieFollowsBobRelationship = new Relationship(charlie, bob);
		relationshipService.saveRelationship(charlieFollowsAliceRelationship);
		relationshipService.saveRelationship(charlieFollowsBobRelationship);

		List<Tweet> charlieWall = tweetService.getWallForUser(charlie);

		assertEquals(4, charlieWall.size(), "Wall should have 4 tweets");
		assertEquals(CHARLIE_TWEET_CONTENT, charlieWall.get(0).getContent(), "Tweet content should be expected value");
		assertEquals(BOB_TWEET_CONTENT_2, charlieWall.get(1).getContent(), "Tweet content should be expected value");
		assertEquals(BOB_TWEET_CONTENT_1, charlieWall.get(2).getContent(), "Tweet content should be expected value");
		assertEquals(ALICE_TWEET_CONTENT, charlieWall.get(3).getContent(), "Tweet content should be expected value");
	}

	@AfterEach
	void cleanup() {
		tweetRepository.deleteAll();
		relationshipRepository.deleteAll();
		userRepository.deleteAll();
	}

}
