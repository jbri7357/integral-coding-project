package com.integral.microblog.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.integral.microblog.model.Relationship;
import com.integral.microblog.model.User;

public interface RelationshipRepository extends CrudRepository<Relationship, Long> {
	List<Relationship> findAllByFollower(User user);
}
