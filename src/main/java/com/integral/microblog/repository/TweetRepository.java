package com.integral.microblog.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.integral.microblog.model.Tweet;
import com.integral.microblog.model.User;

public interface TweetRepository extends CrudRepository<Tweet, Long> {
	List<Tweet> findAllByUser(User user);
}
