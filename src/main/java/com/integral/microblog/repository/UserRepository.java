package com.integral.microblog.repository;

import org.springframework.data.repository.CrudRepository;

import com.integral.microblog.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
