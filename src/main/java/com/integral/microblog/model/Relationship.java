package com.integral.microblog.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Relationship implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
	@JoinColumn(name = "follower_id")
	private User follower;

	@ManyToOne
	@JoinColumn(name = "followee_id")
	private User followee;

	Relationship() {

	}

	public Relationship(User follower, User followee) {
		this.follower = follower;
		this.followee = followee;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getFollower() {
		return follower;
	}

	public User getFollowee() {
		return followee;
	}

}
