package com.integral.microblog.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;

	@NotNull
	private String email;

	@NotNull
	private String name;

	@OneToMany(mappedBy = "follower")
	private List<Relationship> followers;

	@OneToMany(mappedBy = "followee")
	private List<Relationship> followees;

	@OneToMany(mappedBy = "user")
	private List<Tweet> tweets;

	User() {

	}

	public User(String email, String name) {
		this.email = email;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public List<Relationship> getFollowers() {
		return followers;
	}

	public List<Relationship> getFollowees() {
		return followees;
	}

	public List<Tweet> getTweets() {
		return tweets;
	}

}
