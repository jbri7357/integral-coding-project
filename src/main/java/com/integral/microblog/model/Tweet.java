package com.integral.microblog.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Tweet implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;

	@NotNull
	private Date creationDate;

	@NotBlank
	@Size(max = 140)
	@Column(length = 140)
	private String content;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	Tweet() {

	}

	public Tweet(User user, Date creationDate, String content) {
		this.user = user;
		this.creationDate = creationDate;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public String getContent() {
		return content;
	}

}
