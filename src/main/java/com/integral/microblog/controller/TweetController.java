package com.integral.microblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.integral.microblog.model.Tweet;
import com.integral.microblog.service.TweetService;

@RestController
@RequestMapping("tweet")
public class TweetController {
	@Autowired
	TweetService tweetService;

	@RequestMapping(method = RequestMethod.POST)
	Tweet postTweet(@RequestBody Tweet tweet) {
		return tweetService.postTweet(tweet);
	}
}
