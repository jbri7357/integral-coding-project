package com.integral.microblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.integral.microblog.model.Relationship;
import com.integral.microblog.repository.RelationshipRepository;

@Service
@Transactional
public class RelationshipService {
	@Autowired
	RelationshipRepository relationshipRepository;

	public Relationship saveRelationship(Relationship relationship) {
		return relationshipRepository.save(relationship);
	}
}
