package com.integral.microblog.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.integral.microblog.model.Relationship;
import com.integral.microblog.model.Tweet;
import com.integral.microblog.model.User;
import com.integral.microblog.repository.RelationshipRepository;
import com.integral.microblog.repository.TweetRepository;

@Service
@Transactional
public class TweetService {
	@Autowired
	TweetRepository tweetRepository;

	@Autowired
	RelationshipRepository relationshipRepository;

	public Tweet postTweet(Tweet tweet) {
		return tweetRepository.save(tweet);
	}

	public List<Tweet> getTimelineForUser(User user) {
		return tweetRepository.findAllByUser(user);
	}

	public List<Tweet> getWallForUser(User user) {
		List<Tweet> wall = new ArrayList<>();

		List<Relationship> relationships = relationshipRepository.findAllByFollower(user);

		// Add tweets of all followees to wall
		relationships.forEach(r -> {
			wall.addAll(getTimelineForUser(r.getFollowee()));
		});

		// Add own users tweets to wall
		wall.addAll(getTimelineForUser(user));

		// Sort wall's tweets by date newest to oldest
		wall.sort((t1, t2) -> t1.getCreationDate().compareTo(t2.getCreationDate()));
		Collections.reverse(wall);

		return wall;
	}
}
